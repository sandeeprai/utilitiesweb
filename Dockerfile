FROM openjdk:8-jre-alpine

WORKDIR '/app'

COPY target/gs-uploading-files-0.1.0.jar ./app.jar

CMD java -jar app.jar